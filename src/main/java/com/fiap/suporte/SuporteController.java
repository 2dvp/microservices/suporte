package com.fiap.suporte;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SuporteController {

    @Autowired
    SuporteService suporteService;

    @GetMapping("/suporte")
    private List<Suporte> getAllSuporte() {
        return suporteService.getAllSuporte();
    }

    @GetMapping("/suporte/{id}")
    private Suporte getSuporte(@PathVariable("id") int id) {
        return suporteService.getSuporteById(id);
    }

    @DeleteMapping("/suporte/{id}")
    private void deleteSuporte(@PathVariable("id") int id) {
      suporteService.delete(id);
    }

    @PostMapping("/suporte")
    private int saveSuporte(@RequestBody Suporte suporte) {
      suporteService.saveOrUpdate(suporte);
        return suporte.getId();
    }
}