package com.fiap.suporte;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SuporteService {
    @Autowired
    SuporteRepository suporteRepository;

    public List<Suporte> getAllSuporte() {
        List<Suporte> suportes = new ArrayList<Suporte>();
        suporteRepository.findAll().forEach(suporte -> suportes.add(suporte));
        return suportes;
    }

    public Suporte getSuporteById(int id) {
        return suporteRepository.findById(id).get();
    }

    public void saveOrUpdate(Suporte suporte) {
        suporteRepository.save(suporte);
    }



    public void delete(int id) {
        suporteRepository.deleteById(id);
    }
}