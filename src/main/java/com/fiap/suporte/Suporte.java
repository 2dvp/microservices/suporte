package com.fiap.suporte;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SUPORTE")
public class Suporte {
    @Id
    @GeneratedValue
    public int id;
    public int user_id; 
    public String ticket_desc;
    public Integer getId() {
       return id;
   }  
}