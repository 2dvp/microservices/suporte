package com.fiap.suporte;

import org.springframework.data.repository.CrudRepository;

public interface SuporteRepository extends CrudRepository<Suporte, Integer> {

}   